$(function () {
    var api_url = 'https://searchy-api.herokuapp.com/';
    //var api_url = 'http://localhost:3000/';
    var getData = function (link) {
        preloader.on();
        $.ajax({
            url: link,
            dataType: 'json',
            type: 'get',
            crossDomain: true,
            success: function (response) {
                preloader.off();
                if (response.length === 0) {
                    $('#counter-results strong').html('No se encontraron resultados!');
                    $('#counter-results').addClass('alert-danger').removeClass('alert-success');
                    $('#messages-container').removeClass('hide');
                }
                else {
                    $('#counter-results strong').html(response.length + ' resultados encontrados!');
                    $('#counter-results').addClass('alert-success').removeClass('alert-danger');
                    $.each(response, function (i, val) {
                        var li = "<li class='list-group-item'>";
                        //li += '<div class="row-picture"><img class="circle" src="http://lorempixel.com/56/56/business/'+ val.nombre+'" alt="'+ val.nombre +'"></img></div>';
                        li += '<div class="row-picture"><img class="circle" src="./images/avatar1.jpg"></img></div>';
                        li += '<div class="row-action-primary"><h4 class="list-group-item-heading">' + val.nombre + '</h4></div>';
                        li += '<div class="row-content hide">';
                        li += '<label>Direccion: </label><br/>';
                        li += '<p class="list-group-item-text">' + val.direccion + '</p><br/>';
                        li += '<label>Telefono: </label><br/>';
                        li += '<span class="list-group-item-text number-client"><i class="mdi-communication-phone"></i> ' + val.numero + '</span>';
                        li += '</div>'
                        li += '</li>';
                        $('#list-items').append(li);
                        setTimeout(function () {
                            $('#list-items li').eq(i).addClass('center');
                        }, i * 200);
                    });
                }
            }
        });
    }

    //
    $('body').on('click', '#list-items .list-group-item', function () {
        $(this).children('.row-content').toggleClass('hide');
    });

    //
    $('#filter_text')
        .on('keypress', function () {
            console.log($(this).val());
        })
        .on('focus', function () {
            $('#filter_text').addClass('input-large');
        })
        .on('blur', function () {
            $('#filter_text').removeClass('input-large');
        });

    //
    $('#search-form').on('submit', function (e) {
        e.preventDefault();
        var match = $('#filter_text').val();
        $('#list-items').children().remove();
        if (match != '') {
            $('#results-container').removeClass('hide');
            $('#messages-container').addClass('hide');
            getData(api_url + 'lookfor/' + match);
        }
        else {
            $('#results-container').addClass('hide');
            $('#messages-container').removeClass('hide');
        }
    });

    //
    preloader = new $.materialPreloader({
        position: 'top',
        height: '7.3em',
        width: '100%',
        col_1: '#159756',
        col_2: '#da4733',
        col_3: '#3b78e7',
        col_4: '#fdba2c',
        fadeIn: 100,
        fadeOut: 100
    });

}); // Final